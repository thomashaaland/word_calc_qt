#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <QString>
#include <QtDebug>

// Function to read from terminal and return a string
// This should not be able to throw exceptions
QString get_command() noexcept
{
    std::string raw_string;
    std::getline(std::cin, raw_string);
    return QString::fromStdString(raw_string);
}

// Parse the command into its elements
// If we have something different to three elements throw an exception
std::vector<QString> parse_command(QString command)
{
    std::vector<QString> strings;
    std::string token;
    std::istringstream sstring(command.toStdString());

    while(sstring >> token)
    {
        strings.push_back(QString::fromStdString(token));
    }

    if (strings.size() != 3) throw "Error: You need the format: [number operation number]";

    return strings;
}

// Function to turn string numbers to int
// Checks to see if the statement is a number and return that number
// If no valid number is found throw an exception
float to_number(QString raw_number)
{
    if ((raw_number[0]).isNumber()) throw "Error: Only accepts words, no actual numbers.";
    if (raw_number.compare("zero") == 0) return 0;
    if (raw_number.compare("one") == 0) return 1;
    if (raw_number.compare("two") == 0) return 2;
    if (raw_number.compare("three") == 0) return 3;
    if (raw_number.compare("four") == 0) return 4;
    if (raw_number.compare("five") == 0) return 5;
    if (raw_number.compare("six") == 0) return 6;
    if (raw_number.compare("seven") == 0) return 7;
    if (raw_number.compare("eight") == 0) return 8;
    if (raw_number.compare("nine") == 0) return 9;
    if (raw_number.compare("ten") == 0) return 10;
    else throw "Error: Only accepts numbers between one and ten.";
}

// Function which performs operations.
// Checks to see if operation is good, if it is not throws and exception
float do_operation(QString command)
{
    std::vector<QString> parsed;
    try 
    {
        parsed = parse_command(command);
    } 
    catch (const char * ex)
    {
        throw ex;
    }
    float num1, num2;
    try 
    {
        num1 = to_number(parsed[0]);
        num2 = to_number(parsed[2]);
    } 
    catch (const char * ex) 
    {
        throw ex;
    }

    if (parsed[1].compare("plus") == 0) return num1 + num2;
    if (parsed[1].compare("minus") == 0) return num1 - num2;
    if (parsed[1].compare("times") == 0) return num1 * num2;
    if (parsed[1].compare("divide") == 0)
    {
        if (num2 == 0) throw "Error: Cannot divide by zero";
        return num1 / num2;
    }
    else
    {
        throw "Error: Only supports the operations [plus, minus, times and divide]";
    }
}

// Top function responsible for running the program.
// This function catches all expected exceptions and tries to handle them
// and ensures the program can run smoothly.
void run() noexcept
{
    qDebug() << "Enter a mathematical expression: ";
    QString command;
    float result;
    while (true)
    {
        command = get_command();
        // Check if the calculator needs to quit
        if (command.compare("q") == 0) return;
        try
        {
            result = do_operation(command);
            std::cout << "The result is: " << result << std::endl;
            std::cout << "Try again: ";
        } 
        catch (const char * ex)
        {
            std::cout << ex << std::endl;
            std::cout << "Try again or press 'q' to quit: ";
        }
        catch (...)
        {
            std::cout << "Error: Something unexpected went wrong!" << std::endl;
            exit(-1);
        }
    }
}

int main()
{
    run();
    return 0;
}
